/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.componentesJSF.beans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Andy Sánchez Gonzalez
 */
@Named
@ViewScoped
public class ComponenteMB implements Serializable {
    
    private String mensaje;
    private String color;
    
    @PostConstruct
    public void init(){
        mensaje = "Componentes JSF";
        color = "black";
    }
    
    public void apagar(){
        mensaje = "Apagando";
        color = "red";
    }
    
    public void acelerar(){
        mensaje = "Acelerando";
        color = "blue";
    }
    
    public void iniciar(){
        mensaje = "Componentes JSF";
        color = "black";
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    
    
}
